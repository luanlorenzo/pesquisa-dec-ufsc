import  paramiko
from scp import SCPClient

def createSSHClient(server, port, user, password):
    client = paramiko.SSHClient()
    client.load_system_host_keys()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(server, port, user, password)
    return client

if __name__ == '__main__':
    server = 'server'
    port = '2200'
    user =  'user'
    password = 'password'
    path = 'public_html'    
    
    ssh = createSSHClient(server, port, user, password)
    scp = SCPClient(ssh.get_transport())

